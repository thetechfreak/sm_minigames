#pragma semicolon 1
#pragma tabsize 0

#include <sourcemod>
#include <socket>
#include <sdktools>

#define PLUGIN_VERSION "0.1"


new Handle:clientSocket;
bool connected;



public Plugin myinfo = 
{
	name = "SM CV",
	author = "techfreak",
	description = "[ANY]",
	version = PLUGIN_VERSION,
	url = "http://www.sourcemod.net/"
};

public void OnPluginStart()
{
	//RegConsoleCmd("changelog", Command_ReadChangelog, ADMFLAG_RCON, "Changelog menu");
	RegAdminCmd("add_mod", Command_ListMembers, ADMFLAG_RCON, "Add Moderator menu");
	RegAdminCmd("add_cv", Command_ListGuests, ADMFLAG_RCON, "Add CV Member menu");

	connected = false;
	clientSocket = SocketCreate(SOCKET_TCP, OnClientSocketError);
	SocketSetOption(clientSocket, SocketReuseAddr, 1);
	LogMessage("Attempt to connect to %s:%i ...", "84.200.226.23", 5000);
	SocketConnect(clientSocket, OnClientSocketConnected, OnChildSocketReceive, OnChildSocketDisconnected, "84.200.226.23", 5000);
}


/*
 * Sockets stuff
 *
*/


public OnClientSocketConnected(Handle socket, any arg)
{
LogMessage("bla");
connected = true;
PrintToServer(">>> CONNECTED !");
SocketSend(clientSocket, ">>> Hello :3");
}

public OnClientSocketError(Handle socket, const int errorType, const int errorNum, any ary)
{
connected = false; //Client NOT connected anymore, this is very important.
LogError("socket error %d (errno %d)", errorType, errorNum);
CloseHandle(socket);
}

public OnChildSocketReceive(Handle socket, char[] receiveData, const int dataSize, any hFile)
{
PrintToServer(">>> %s", receiveData);
}

public OnChildSocketDisconnected(Handle socket, any hFile)
{
PrintToServer(">>> DISCONNECTED !");
connected = false;
CloseHandle(socket);
}

/*
 * Add Member menu
 *
*/




public void OnClientPostAdminCheck(client){
	decl String:player_authid[32];
    decl String:SteamID[25];
	decl String:SteamName[255];
	new bool:newUser = true;

    if(	GetClientAuthId(client, AuthId_Steam3, player_authid, sizeof(player_authid)) )
	{
	
		decl String:path[PLATFORM_MAX_PATH];
		BuildPath(Path_SM, path, sizeof(path), "configs/admins.cfg");
		
		KeyValues kv = new KeyValues("Admins");
		FileToKeyValues(kv, path);	
		KvGotoFirstSubKey(kv);
		
		do
		{
			KvGetString(kv, "identity",SteamID, sizeof(SteamID));
				
			if (StrEqual(player_authid, SteamID)){
				newUser = false;
			}
		} while (
			kv.GotoNextKey()
		);
		
		if(newUser == true)
		{
			GetClientName(client,SteamName,sizeof(SteamName));
			KvRewind(kv);
			KvJumpToKey(kv, SteamName,true);
			KvSetSectionName(kv, SteamName);
			KvSetString(kv,"auth", "steam");
			KvSetString(kv,"identity", player_authid);
			KvSetString(kv,"group", "guest");
			KvRewind(kv);
			KeyValuesToFile(kv,path);
		}
	}
}

/*
 * Add Member menu
 *
*/

public Action:Command_ListGuests(client, args){

	decl String:path[PLATFORM_MAX_PATH];
    decl String:SteamID[255];
    decl String:SteamName[255];	
    decl String:Group[255];	

	BuildPath(Path_SM, path, sizeof(path), "configs/admins.cfg"); 
	
	KeyValues kv = new KeyValues("Admins");
	FileToKeyValues(kv, path);
	if (!KvGotoFirstSubKey(kv))
	{
		return Plugin_Continue;
	}
	
	
	Menu menu = new Menu(AddMemberMenuHandler);
	menu.SetTitle("Add Member");

	do
	{
        KvGetString(kv, "group", Group, sizeof(Group));
		KvGetString(kv, "identity", SteamID, sizeof(SteamID));
				
		if (StrEqual("guest", Group)){
			KvGetSectionName(kv, SteamName, sizeof(SteamName));
			menu.AddItem(SteamName,SteamName);
		}
    }while (KvGotoNextKey(kv));
	menu.Display(client, 20);
	menu.ExitButton = true;
	
	return Plugin_Handled;
}

/*
 * Add Moderator menu
 *
*/


public Action:Command_ListMembers(client, args){

	decl String:path[PLATFORM_MAX_PATH];
    decl String:SteamID[255];
    decl String:SteamName[255];	
    decl String:Group[255];	

	BuildPath(Path_SM, path, sizeof(path), "configs/admins.cfg"); 
	
	KeyValues kv = new KeyValues("Admins");
	FileToKeyValues(kv, path);	
	
	if (!KvGotoFirstSubKey(kv))
	{
		return Plugin_Continue;
	}
	
	Menu menu = new Menu(AddModMenuHandler);
	menu.SetTitle("Add Moderator");
	do
	{

        KvGetString(kv, "group", Group, sizeof(Group));
		KvGetString(kv, "identity", SteamID, sizeof(SteamID));
				
		if (StrEqual("cv_member", Group)){
			KvGetSectionName(kv, SteamName, sizeof(SteamName));    
			menu.AddItem(SteamName,SteamName);
		}
    }while (KvGotoNextKey(kv));
	
	menu.ExitButton = true;
	menu.Display(client, 20);
	
	return Plugin_Handled;
}



/*
 * Menuhandlers
 *
*/

public int AddModMenuHandler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{

        decl String:choice[255];
		decl String:path[PLATFORM_MAX_PATH];
		
		BuildPath(Path_SM, path, sizeof(path), "configs/admins.cfg"); 
	
		KeyValues kv = new KeyValues("Admins");		
		FileToKeyValues(kv, path);	

		GetMenuItem(menu, param2, choice, sizeof(choice));     
		
		kv.JumpToKey(choice, true);
		KvSetString(kv,"group", "cv_moderator");
		KvRewind(kv);
		KeyValuesToFile(kv,path);
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public int AddMemberMenuHandler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
        decl String:choice[255];
		decl String:path[PLATFORM_MAX_PATH];
		
		BuildPath(Path_SM, path, sizeof(path), "configs/admins.cfg"); 
	
		KeyValues kv = new KeyValues("Admins");		
		FileToKeyValues(kv, path);	

		GetMenuItem(menu, param2, choice, sizeof(choice));     
		
		kv.JumpToKey(choice, true);
		KvSetString(kv,"group", "cv_member");
		KvRewind(kv);
		KeyValuesToFile(kv,path);
	}
	/* If the menu has ended, destroy it */
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}